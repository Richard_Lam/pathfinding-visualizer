var delay = 1;

function startDepthFirstSearch() {
    GRID.clearVisitedCells();
    stopAllPathfindingAlgorithms();
    DEPTH_FIRST_SEARCH.startAlgorithm();
}

function startBreadthFirstSearch() {
    GRID.clearVisitedCells();
    stopAllPathfindingAlgorithms();
    BREADTH_FIRST_SEARCH.startAlgorithm();
}

function startAStarSearch() {
    GRID.clearVisitedCells();
    stopAllPathfindingAlgorithms();
    A_STAR_SEARCH.startAlgorithm();
}

function startBiDirectionalSearch() {
    GRID.clearVisitedCells();
    stopAllPathfindingAlgorithms();
    BI_DIRECTIONAL_SEARCH.startAlgorithm();
}

function selectDelayOption(selectedDelay) {
    delay = selectedDelay;
    DELAY_MENU.selectDelayOption(selectedDelay);
    DELAY_MENU.hideDelayOptions();
}

function clearGrid() {
    GRID.clearAll();
    stopAllPathfindingAlgorithms();
}

function onLoad() {
    GRID.generateGrid();
    EVENT_HANDLERS.addEventListeners();
}
onLoad();