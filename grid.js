// Dependencies: EVENT_HANDLERS module

var GRID = (function(){
    let PUBLIC_METHODS = {};

    let GRID_HEIGHT = 17;
    let GRID_LENGTH = 45;
    let startingSquare = "8-22";
    let targetSquare = "14-26";
    let renderOptimalPath = true;

    PUBLIC_METHODS.generateGrid = function() {
        let grid = document.getElementById("grid");
    
        for (let i=0; i < GRID_HEIGHT; i++) {
            let row = "<tr>";
            for (let j=0; j < GRID_LENGTH; j++) {
                let currentSquare = `${i}-${j}`;
                if (currentSquare == startingSquare) {
                    row += `<td class='start ${currentSquare}' onClick='EVENT_HANDLERS.onMouseClick("${currentSquare}");' onMouseOver='EVENT_HANDLERS.onMouseOver("${currentSquare}");'></td>`;
                }
                else if (currentSquare == targetSquare) {
                    row += `<td class='target ${currentSquare}' onClick='EVENT_HANDLERS.onMouseClick("${currentSquare}");' onMouseOver='EVENT_HANDLERS.onMouseOver("${currentSquare}");'></td>`;
                }
                else {
                    row += `<td class='${currentSquare}' onClick='EVENT_HANDLERS.onMouseClick("${currentSquare}");' onMouseOver='EVENT_HANDLERS.onMouseOver("${currentSquare}");'></td>`;
                }
            }
            row += "</tr>";
            grid.innerHTML += row;
        }
    }

    PUBLIC_METHODS.clearVisitedCells = function() {
        let visitedCells = document.getElementsByClassName("visited");
        for (let i=0; i < visitedCells.length; i++) {
            if (visitedCells[i]) {
                visitedCells[i].classList.remove("optimal-path");
                visitedCells[i].classList.remove("visited");
                i--;
            }
        }
    }

    PUBLIC_METHODS.getStartingSquare = function() {
        return startingSquare;
    }
    PUBLIC_METHODS.getTargetSquare = function() {
        return targetSquare;
    }
    PUBLIC_METHODS.getGridLength = function() {
        return GRID_LENGTH;
    }
    PUBLIC_METHODS.getGridHeight = function() {
        return GRID_HEIGHT;
    }

    PUBLIC_METHODS.renderOptimalPath = function(path) {

        function renderNextSquare(path, i) {
            if (renderOptimalPath == false) {
                return;
            }
            if (i < path.length) {
                let element = document.getElementsByClassName(path[i]);
                element[0].classList.add("optimal-path");
                i++;
                setTimeout(()=>renderNextSquare(path, i), delay);
            }
        }
        renderOptimalPath = true;
        renderNextSquare(path, 0);
    }

    PUBLIC_METHODS.stopRenderingOptimalPath = function() {
        renderOptimalPath = false;
    }

    PUBLIC_METHODS.changeToWall = function(square) {
        let squareElement = document.getElementsByClassName(square)[0];
        
        if (square == startingSquare || square == targetSquare) {
            return;
        }

        if (squareElement.classList.contains("wall")) {
            squareElement.classList.remove("wall");
        }
        else {
            if (square != targetSquare) {
                squareElement.classList.add("wall");
            }
        }
    }

    PUBLIC_METHODS.removeWall = function(square) {
        let squareElement = document.getElementsByClassName(square)[0];
        if (squareElement.classList.contains("wall")) {
            squareElement.classList.remove("wall");
        }
    }

    PUBLIC_METHODS.removeWalls = function() {
        clearElementsWithClassname("wall");
    }

    PUBLIC_METHODS.clearAll = function() {
        PUBLIC_METHODS.removeWalls();
        clearElementsWithClassname("pathway");
        clearElementsWithClassname("optimal-path");
        clearElementsWithClassname("visited");
    }

    function clearElementsWithClassname(className) {
        let elements = document.getElementsByClassName(className);
        while (elements.length > 0) {
            elements[0].classList.remove(className);
        }
    }

    PUBLIC_METHODS.changeTargetSquare = function(square) {
        document.getElementsByClassName(targetSquare)[0].classList.remove("target");
        targetSquare = square;
        document.getElementsByClassName(targetSquare)[0].classList.add("target");
    }

    PUBLIC_METHODS.changeStartingSquare = function(square) {
        document.getElementsByClassName(startingSquare)[0].classList.remove("start");
        startingSquare = square;
        document.getElementsByClassName(startingSquare)[0].classList.add("start");
    }

    return PUBLIC_METHODS;
}());