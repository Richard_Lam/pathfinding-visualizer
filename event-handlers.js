// Dependencies: GRID module

var EVENT_HANDLERS = (function(){

    let PUBLIC_METHODS = {};

    let MOUSE_DOWN = false;
    let CLICKED_TARGET_SQUARE = false;
    let CLICKED_STARTING_SQUARE = false;

    PUBLIC_METHODS.addEventListeners = function() {
        document.body.onmousedown = function() {
            MOUSE_DOWN = true;
        }
        document.body.onmouseup = function() {
            MOUSE_DOWN = false;
        }
    }

    PUBLIC_METHODS.onMouseOver = function(square) {
        if (CLICKED_TARGET_SQUARE) {
            GRID.changeTargetSquare(square);
        }
        if (CLICKED_STARTING_SQUARE) {
            GRID.changeStartingSquare(square);
        }
        if (MOUSE_DOWN) {
            GRID.changeToWall(square);
        }
    }

    PUBLIC_METHODS.onMouseClick = function(square) {
        if (CLICKED_TARGET_SQUARE) {
            CLICKED_TARGET_SQUARE = false;
        }
        else {
            CLICKED_TARGET_SQUARE = document.getElementsByClassName(square)[0].classList.contains("target");
        }
        if (CLICKED_STARTING_SQUARE) {
            CLICKED_STARTING_SQUARE = false;
        }
        else {
            CLICKED_STARTING_SQUARE = document.getElementsByClassName(square)[0].classList.contains("start");
        }
        if (!CLICKED_TARGET_SQUARE) {
            GRID.changeToWall(square);
        }
    }

    return PUBLIC_METHODS;
}());