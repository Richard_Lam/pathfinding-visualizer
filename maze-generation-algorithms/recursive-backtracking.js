// Dependencies: GRID module

var RECURSIVE_BACKTRACKING = (function(){

    let PUBLIC_METHODS = {};

    let stack = [];

    PUBLIC_METHODS.generateMaze = function() {
        
        let startSquare = GRID.getStartingSquare();

        GRID.clearAll();
        stack.push(startSquare);
        executeWhileStackIsNotEmpty();
    }

    // Pick random unvisited neighbor and set as pathway, push onto stack, if no neighbors, pop off stack
    function executeWhileStackIsNotEmpty() {
        if (stack.length > 0) {
            let currentSquare = stack[stack.length-1];
            let neighborNodes = getUnvisitedNeighborNodes(currentSquare);
            if (neighborNodes.length > 0) {
                let squarePair = selectRandomPair(neighborNodes);
                setSquareAsPathway(squarePair[0]);
                setSquareAsPathway(squarePair[1]);
                let newSquare = squarePair[1];
                stack.push(newSquare);
            }
            else {
                stack.pop();
            }
            setTimeout(()=>executeWhileStackIsNotEmpty(), delay);
        }
        else {
            setNonpathwaysAsWalls();
            removePathways();
        }
    }

    function getUnvisitedNeighborNodes(square) {

        let squares = getNeighborNodes(square);

        let validUnvisitedSquares = [];
        for (let i=0; i < squares.length; i+=2) {
            let squarePairs = [squares[i], squares[i+1]];
            let validSquares = 0;
            for (let j=0; j < squarePairs.length; j++) {
                let currentSquare = squarePairs[j];
                if (isSquareValid(currentSquare) && !isSquareAPathway(currentSquare)) {
                    validSquares += 1;
                }
            }
            // If both squares are valid, push to list
            if (validSquares == 2) {
                validUnvisitedSquares.push(squares[i]);
                validUnvisitedSquares.push(squares[i+1]);
            }
        }
        return validUnvisitedSquares;
    }

    function getNeighborNodes(square) {
        square = square.split("-");
        let left_1 = `${square[0]}-${square[1]*1-1}`;
        let left_2 = `${square[0]}-${square[1]*1-2}`;
        let right_1 = `${square[0]}-${square[1]*1+1}`;
        let right_2 = `${square[0]}-${square[1]*1+2}`;
        let top_1 = `${square[0]*1-1}-${square[1]}`;
        let top_2 = `${square[0]*1-2}-${square[1]}`;
        let bottom_1 = `${square[0]*1+1}-${square[1]}`;
        let bottom_2 = `${square[0]*1+2}-${square[1]}`;
        return [left_1, left_2, right_1, right_2, top_1, top_2, bottom_1, bottom_2];
    }

    function selectRandomPair(arrOfNeighbors) {
        let randomNumber = generateRandomNumber(0, (arrOfNeighbors.length/2)-1)*2;
        return [arrOfNeighbors[randomNumber], arrOfNeighbors[randomNumber+1]];
    }

    function setSquareAsPathway(square) {
        let squareElement = document.getElementsByClassName(square)[0];
        squareElement.classList.add("pathway");
    }

    function isSquareAPathway(square) {
        let element = document.getElementsByClassName(square)[0];
        if (element) {
            return element.classList.contains("pathway");   
        }
        return false;
    }

    function isSquareValid(square) {
        square = square.split("-");
        if (square.length != 2) {
            return false;
        }
        let rowIndexIsInBounds = square[0]*1 < GRID.getGridHeight();
        let columnIndexIsInBounds = square[1]*1 < GRID.getGridLength();
        return rowIndexIsInBounds && columnIndexIsInBounds;
    }

    function setNonpathwaysAsWalls() {
        let gridLength = GRID.getGridLength();
        let gridHeight = GRID.getGridHeight();
        for (let i=0; i < gridLength; i++) {
            for (let j=0; j < gridHeight; j++) {
                let square = `${j}-${i}`;
                let element = document.getElementsByClassName(square)[0];
                if (!element.classList.contains("pathway")) {
                    GRID.changeToWall(square);
                }
            }
        }
    }

    function removePathways() {
        let pathwayElements = document.getElementsByClassName("pathway");
        while (pathwayElements.length > 0) {
            pathwayElements[0].classList.remove("pathway");
        }
    }

    return PUBLIC_METHODS;

}());