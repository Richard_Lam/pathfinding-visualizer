// Dependencies: GRID module

// To be completed

var RECURSIVE_DIVISION = (function(){
    
    let PUBLIC_METHODS = {};

    PUBLIC_METHODS.generateMaze = function() {
        let gridLength = GRID.getGridLength();
        let gridHeight = GRID.getGridHeight();
        recursiveDivision(gridLength, gridHeight);
    }

    function recursiveDivision(gridLength, gridHeight) {
        divideMazeInHalf(0, 0, gridLength-1, gridHeight-1);
    }

    let count = 0;
    let stop = 100;
    function divideMazeInHalf(grid_x, grid_y, grid_x2, grid_y2) {
        if (grid_x2 <= grid_x || grid_y2 <= grid_y) {
            return;
        }
        let delay = 1;
        let randomNumber = Math.floor(Math.random()*2);
        // let randomNumber = 1;
        if (randomNumber == 0) {
            let wall_x = generateRandomNumber(grid_x+2, grid_x2-2);
            let hole_y = generateVerticalWall(grid_y, wall_x, grid_y2);                
            console.log(wall_x);
            if (count < stop) {
                count++;
                // https://www.youtube.com/watch?v=1GENvb4y95s
                setTimeout(()=>divideMazeInHalf(grid_x, grid_y, wall_x-1, grid_y2), delay);
                setTimeout(()=>divideMazeInHalf(wall_x+1, grid_y, grid_x2, grid_y2), delay);
            }
        }
        else {
            let wall_y = generateRandomNumber(grid_y+2, grid_y2-2);
            let hole_x = generateHorizontalWall(grid_x, wall_y, grid_x2);
            if (count < stop) {
                count++;
                setTimeout(()=>divideMazeInHalf(grid_x, grid_y, grid_x2, wall_y-1), delay);
                setTimeout(()=>divideMazeInHalf(grid_x, wall_y+1, grid_x2, grid_y2), delay);
            }
        }
    }

    function generateVerticalWall(grid_y, wall_x, grid_y2) {
        for (let i=grid_y; i <= grid_y2; i++) {
            let square = `${i}-${wall_x}`;
            GRID.changeToWall(square);
        }
        let openingInWall_y = generateRandomNumber(grid_y, grid_y2);
        let openingInWall = `${openingInWall_y}-${wall_x}`;
        GRID.removeWall(openingInWall);
        return openingInWall_y;
    }

    function generateHorizontalWall(grid_x, wall_y, grid_x2) {
        for (let i=grid_x; i <= grid_x2; i++) {
            let square = `${wall_y}-${i}`;
            GRID.changeToWall(square);
        }
        let openingInWall_x = generateRandomNumber(grid_x, grid_x2);
        let openingInWall = `${wall_y}-${openingInWall_x}`;
        GRID.removeWall(openingInWall);
        return openingInWall_x;
    }

    return PUBLIC_METHODS;
}());