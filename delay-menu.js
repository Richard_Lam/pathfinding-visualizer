var DELAY_MENU = (function(){
    let PUBLIC_METHODS = {};
    let delayOptionsVisible = false;

    PUBLIC_METHODS.toggleDelayOptions = function() {
        if (delayOptionsVisible) {
            PUBLIC_METHODS.hideDelayOptions();
        }
        else {
            showDelayOptions();
        }
    }

    function showDelayOptions() {
        let delaySelector = document.getElementsByClassName("delay-selector")[0];
        let delaySelectorRect = delaySelector.getBoundingClientRect();
        let delayOptions = document.getElementById("delay-options");
        delayOptions.style.display = "flex";
        delayOptions.style.top = (delaySelectorRect.top+ delaySelectorRect.height + 15) + "px" ;
        delayOptions.style.left = delaySelectorRect.left + "px";
        delayOptionsVisible = true;
    }

    PUBLIC_METHODS.hideDelayOptions = function() {
        let delayOptions = document.getElementById("delay-options");
        delayOptions.style.display = "none";
        delayOptionsVisible = false;
    }

    PUBLIC_METHODS.selectDelayOption = function(selectedDelay) {
        unselectDelayOptions();
        let elementID = null;
        if (selectedDelay == 1) {
            elementID = "1ms-delay";
        }
        else if (selectedDelay == 10) {
            elementID = "10ms-delay";
        }
        else if (selectedDelay == 20) {
            elementID = "20ms-delay";
        }
        else if (selectedDelay == 50) {
            elementID = "50ms-delay";
        }
        let delayElement = document.getElementById(elementID);
        if (!delayElement.classList.contains("selected")) {
            delayElement.classList.add("selected");
            let delaySelector = document.getElementsByClassName("delay-selector")[0];
            delaySelector.innerHTML = delayElement.innerHTML;
        }
    }

    function unselectDelayOptions() {
        let delayOptions = ["1ms-delay", "10ms-delay", "20ms-delay", "50ms-delay"];
        for (let i=0; i < delayOptions.length; i++) {
            let element = document.getElementById(delayOptions[i]);
            if (element.classList.contains("selected")) {
                element.classList.remove("selected");
            }
        }
    }

    return PUBLIC_METHODS;
}());