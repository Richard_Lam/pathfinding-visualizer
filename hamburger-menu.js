var HAMBURGER_MENU = (function(){
    let PUBLIC_METHODS = {};

    PUBLIC_METHODS.displayMenu = function() {
        let hamburgerMenuElement = document.getElementById("hamburger-menu");
        hamburgerMenuElement.style.width = "220px";
    }

    PUBLIC_METHODS.hideMenu = function() {
        let hamburgerMenuElement = document.getElementById("hamburger-menu");
        hamburgerMenuElement.style.width = "0px";
    }

    return PUBLIC_METHODS;
}()); 