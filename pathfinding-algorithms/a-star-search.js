// Dependencies: GRID module, SQUARE_MANAGEMENT module

var A_STAR_SEARCH = (function(){
    let PUBLIC_METHODS = {};

    let startSquare = "0-0";
    let targetSquare = "0-0";

    let openNodes = [];
    let closedNodes = [];

    let previousSquare = {};
    let executeAlgorithm = false;

    PUBLIC_METHODS.startAlgorithm = function() {
        startSquare = GRID.getStartingSquare();
        targetSquare = GRID.getTargetSquare();

        openNodes = [];
        closedNodes = [];
        openNodes.push({square: startSquare, f_cost: 0});
        SQUARE_MANAGEMENT.setSquareAsVisited(startSquare);

        executeAlgorithm = true;
        executeWhileTargetIsNotFound();
    }

    PUBLIC_METHODS.stopAlgorithm = function() {
        executeAlgorithm = false;
    }

    function executeWhileTargetIsNotFound() {
        if (openNodes.length == 0 || executeAlgorithm == false) {
            return;
        }
        let currentSquare = openNodes[0].square;
        SQUARE_MANAGEMENT.setSquareAsVisited(currentSquare);
        openNodes.shift();
        closedNodes.push(currentSquare);
        if (currentSquare == targetSquare) {
            executeOnceAlgorithmCompletes();
            return;
        }

        let arrOfAdjacentSquares = SQUARE_MANAGEMENT.getAdjacentSquares(currentSquare);

        for (let i=0; i < arrOfAdjacentSquares.length; i++) {
            let adjacentSquare = arrOfAdjacentSquares[i];
            if (isNodeClosed(adjacentSquare)) {
                continue;
            }
            if (!isNodeOpen(adjacentSquare)) {
                let f_cost = calculateFcost(adjacentSquare);
                previousSquare[adjacentSquare] = currentSquare;
                addSquareToOpenNodes(adjacentSquare, f_cost);
            }
        }
        setTimeout(()=>executeWhileTargetIsNotFound(), delay);
    }

    function isNodeClosed(square) {
        for (let i=0; i < closedNodes.length; i++) {
            if (closedNodes[i] == square) {
                return true;
            }
        }
        return false;
    }

    function isNodeOpen(square) {
        for (let i=0; i < openNodes.length; i++) {
            if (openNodes[i].square == square) {
                return true;
            }
        }
        return false;
    }

    function addSquareToOpenNodes(square, f_cost) {
        square = {square: square, f_cost: f_cost};
        for (let i=0; i < openNodes.length; i++) {
            let lowestFcost = openNodes[0].f_cost;
            if (f_cost <= lowestFcost) {
                openNodes.splice(i, 0, square);
                return;
            }
        }
        openNodes.push(square);
    }

    function calculateFcost(square) {
        let g_cost = calculateDistanceBetweenTwoSquares(square, targetSquare);
        let h_cost = calculateDistanceBetweenTwoSquares(square, startSquare);
        let f_cost = g_cost + h_cost;
        return f_cost;
    }

    function calculateDistanceBetweenTwoSquares(square1, square2) {
        square1 = square1.split("-");
        square2 = square2.split("-");
        return ((square1[0] - square2[0])**2 + (square1[1] - square2[1])**2)**.5;
    }

    function executeOnceAlgorithmCompletes() {
        GRID.renderOptimalPath(getOptimalPath());
    }

    function getOptimalPath() {
        let currentPreviousSquare = targetSquare;
        let path = [currentPreviousSquare];
        while (currentPreviousSquare != startSquare) {
            currentPreviousSquare = previousSquare[currentPreviousSquare];
            path.unshift(currentPreviousSquare);
        }
        return path;
    }

    return PUBLIC_METHODS;
}());