// Dependencies: GRID module, SQUARE_MANAGEMENT module

var DEPTH_FIRST_SEARCH = (function(){
    let PUBLIC_METHODS = {};
    let startSquare = "0-0";
    let targetSquare = "0-0";
    let stack = [];
    let previousSquare = {};

    let executeAlgorithm = false;

    PUBLIC_METHODS.startAlgorithm = function() {
        startSquare = GRID.getStartingSquare();
        targetSquare = GRID.getTargetSquare();
        SQUARE_MANAGEMENT.setSquareAsVisited(startSquare);
        stack.push(startSquare);

        executeAlgorithm = true;
        executeWhileStackIsNotEmpty();
    }

    PUBLIC_METHODS.stopAlgorithm = function() {
        executeAlgorithm = false;
    }

    function executeWhileStackIsNotEmpty() {
        if (executeAlgorithm == false) {
            return;
        }
        
        if (stack.length > 0) {
            let topOfStack = stack[stack.length-1];
            if (topOfStack == targetSquare) {
                executeOnceAlgorithmCompletes();
                return;
            }
            if (adjacentSquaresAreVisited(topOfStack)) {
                stack.pop();
            }
            setTimeout(()=>executeWhileStackIsNotEmpty(), delay);
        }
    }

    function adjacentSquaresAreVisited(square) {

        let arrOfAdjacentSquares = SQUARE_MANAGEMENT.getAdjacentSquares(square);
        for (let i=0; i < arrOfAdjacentSquares.length; i++) {
            let currentSquare = arrOfAdjacentSquares[i];
            if (!SQUARE_MANAGEMENT.squareIsVisited(currentSquare)) {
                SQUARE_MANAGEMENT.setSquareAsVisited(currentSquare);
                previousSquare[currentSquare] = square;
                stack.push(currentSquare);
                return false;
            }
        }
        return true;
    }

    function executeOnceAlgorithmCompletes() {
        GRID.renderOptimalPath(getOptimalPath());
    }

    function getOptimalPath() {
        let currentPreviousSquare = targetSquare;
        let path = [currentPreviousSquare];
        while (currentPreviousSquare != startSquare) {
            currentPreviousSquare = previousSquare[currentPreviousSquare];
            path.unshift(currentPreviousSquare);
        }
        return path;
    }

    return PUBLIC_METHODS;
}());