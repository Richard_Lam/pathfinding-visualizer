// Dependencies: GRID module, SQUARE_MANAGEMENT module

var BREADTH_FIRST_SEARCH = (function(){
    let PUBLIC_METHODS = {};
    let startSquare = "0-0";
    let targetSquare = "0-0";
    let queue = [];
    let previousSquare = {};
    
    let executeAlgorithm = false;

    PUBLIC_METHODS.startAlgorithm = function() {
        startSquare = GRID.getStartingSquare();
        targetSquare = GRID.getTargetSquare();
        SQUARE_MANAGEMENT.setSquareAsVisited(startSquare);
        queue = [];
        queue.push(startSquare);

        executeAlgorithm = true;
        executeWhileQueueIsNotEmpty();
    }

    PUBLIC_METHODS.stopAlgorithm = function() {
        executeAlgorithm = false;
    }

    function executeWhileQueueIsNotEmpty() {
        if (executeAlgorithm == false) {
            return;
        }

        if (queue.length > 0) {
            let frontOfQueue = queue[0];
            let lastVisitedSquare = queue[queue.length - 1];
            if (lastVisitedSquare == targetSquare) {
                executeOnceAlgorithmCompletes();
                return;
            }
            if (adjacentSquaresAreVisited(frontOfQueue)) {
                queue.shift();
            }
            setTimeout(()=>executeWhileQueueIsNotEmpty(), delay);
        }
    }

    function adjacentSquaresAreVisited(square) {
        
        let arrOfAdjacentSquares = SQUARE_MANAGEMENT.getAdjacentSquares(square);
        
        for (let i=0; i < arrOfAdjacentSquares.length; i++) {
            let currentSquare = arrOfAdjacentSquares[i];
            if (!SQUARE_MANAGEMENT.squareIsVisited(currentSquare)) {
                SQUARE_MANAGEMENT.setSquareAsVisited(currentSquare);
                previousSquare[currentSquare] = square;
                queue.push(currentSquare);
                return false;
            }
        }
        return true;
    }

    function executeOnceAlgorithmCompletes() {
        GRID.renderOptimalPath(getOptimalPath());
    }

    function getOptimalPath() {
        let currentPreviousSquare = targetSquare;
        let path = [currentPreviousSquare];
        while (currentPreviousSquare != startSquare) {
            currentPreviousSquare = previousSquare[currentPreviousSquare];
            path.unshift(currentPreviousSquare);
        }
        return path;
    }

    return PUBLIC_METHODS;
}());