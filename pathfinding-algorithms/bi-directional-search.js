// Dependencies: GRID module, SQUARE_MANAGEMENT module

var BI_DIRECTIONAL_SEARCH = (function(){

    let PUBLIC_METHODS = {};
    let startQueue = [];
    let targetQueue = [];

    let targetFound = false;
    let intersectionSquare = null;

    let startPreviousSquare = {};
    let targetPreviousSquare = {};

    let executeAlgorithm = false;

    PUBLIC_METHODS.startAlgorithm = function() {

        executeAlgorithm = true;

        startSquare = GRID.getStartingSquare();
        targetSquare = GRID.getTargetSquare();
        
        SQUARE_MANAGEMENT.setSquareAsVisited(startSquare);
        SQUARE_MANAGEMENT.setSquareAsVisited(targetSquare);
        startQueue = [startSquare];
        targetQueue = [targetSquare];

        targetFound = false;

        executeWhileStartQueueisNotEmpty();
        executeWhileTargetQueueisNotEmpy();
    }

    PUBLIC_METHODS.stopAlgorithm = function() {
        executeAlgorithm = false;
    }

    function executeWhileStartQueueisNotEmpty() {
        if (targetFound || executeAlgorithm == false) {
            return;
        }
        if (startQueue.length > 0) {
            let frontOfQueue = startQueue[0];
            if (adjacentSquaresAreVisited(frontOfQueue, "startQueue")) {
                startQueue.shift();
            }
            setTimeout(()=>executeWhileStartQueueisNotEmpty(), delay);
        }
    }

    function executeWhileTargetQueueisNotEmpy() {
        if (targetFound || executeAlgorithm == false) {
            return;
        }
        if (targetQueue.length > 0) {
            let frontOfQueue = targetQueue[0];
            if (adjacentSquaresAreVisited(frontOfQueue, "targetQueue")) {
                targetQueue.shift();
            }
            setTimeout(()=>executeWhileTargetQueueisNotEmpy(), delay);
        }
    }

    function adjacentSquaresAreVisited(square, queueType) {
        
        let arrOfAdjacentSquares = SQUARE_MANAGEMENT.getAdjacentSquares(square);
        let oppositeQueue = queueType == "targetQueue" ? "startQueue" : "targetQueue";
        
        for (let i=0; i < arrOfAdjacentSquares.length; i++) {
            let currentSquare = arrOfAdjacentSquares[i];
            if (!SQUARE_MANAGEMENT.squareIsVisited(currentSquare)) {
                SQUARE_MANAGEMENT.setSquareAsVisited(currentSquare);
                markSquare(currentSquare, queueType);
                pushSquareToQueue(currentSquare, queueType);
                addPreviousSquare(currentSquare, square, queueType);
                return false;
            }
            else if (isSquareFromDifferentQueue(currentSquare, oppositeQueue)) {
                addPreviousSquare(currentSquare, square, queueType);
                intersectionSquare = currentSquare;
                targetFound = true;
                executeOnceAlgorithmCompletes();
                return true;
            }
        }
        return true;
    }

    function pushSquareToQueue(square, queueType) {
        if (queueType == "targetQueue") {
            targetQueue.push(square);
        }
        else if (queueType == "startQueue") {
            startQueue.push(square);
        }
    }

    function addPreviousSquare(currentSquare, square, queueType) {
        if (queueType == "targetQueue") {
            targetPreviousSquare[currentSquare] = square;
        }
        else if (queueType == "startQueue") {
            startPreviousSquare[currentSquare] = square;
        }
    }

    function isSquareFromDifferentQueue(square, oppositeQueue) {
        return document.getElementsByClassName(square)[0].classList.contains(oppositeQueue);
    }

    function markSquare(square, queueType) {
        let squareElement = document.getElementsByClassName(square)[0];
        squareElement.classList.add(queueType);
    }

    function clearSquaresOfMarks() {
        let elements = document.getElementsByClassName("targetQueue");
        while (elements.length > 0) {
            elements[0].classList.remove("targetQueue");
        }
        elements = document.getElementsByClassName("startQueue");
        while (elements.length > 0) {
            elements[0].classList.remove("startQueue");
        }
    }

    function executeOnceAlgorithmCompletes() {
        clearSquaresOfMarks();
        GRID.renderOptimalPath(getOptimalPath());
    }

    function getOptimalPath() {
        
        let currentPreviousSquare = intersectionSquare;
        let path = [currentPreviousSquare];
        while (currentPreviousSquare != startSquare) {
            currentPreviousSquare = startPreviousSquare[currentPreviousSquare];
            path.unshift(currentPreviousSquare);
        }

        currentPreviousSquare = intersectionSquare;
        while (currentPreviousSquare != targetSquare) {
            currentPreviousSquare = targetPreviousSquare[currentPreviousSquare];
            path.push(currentPreviousSquare);
        }
        return path;
    }

    return PUBLIC_METHODS;
}());