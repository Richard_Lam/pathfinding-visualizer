var SQUARE_MANAGEMENT = (function(){

    let PUBLIC_METHODS = {};

    PUBLIC_METHODS.setSquareAsVisited = function(square) {
        let elements = document.getElementsByClassName(square);
        elements[0].classList.add("visited");
    }

    PUBLIC_METHODS.squareIsVisited = function(square) {
        let elements = document.getElementsByClassName(square);
        return elements[0].classList.contains("visited");
    }

    function squareIsNotWall(square) {
        let elements = document.getElementsByClassName(square);
        return !elements[0].classList.contains("wall");
    }

    function squareIsValid(square) {
        square = square.split("-");
        if (square.length != 2) {
            return false;
        }
        let rowIndexIsInBounds = square[0]*1 < GRID.getGridHeight();
        let columnIndexIsInBounds = square[1]*1 < GRID.getGridLength();
        return rowIndexIsInBounds && columnIndexIsInBounds; 
    }

    PUBLIC_METHODS.getAdjacentSquares = function(square) {
        square = square.split("-");

        let topSquare = `${square[0]*1 - 1}-${square[1]}`;
        let rightSquare = `${square[0]}-${square[1]*1 + 1}`;
        let bottomSquare = `${square[0]*1 + 1}-${square[1]}`;
        let leftSquare = `${square[0]}-${square[1]*1 - 1}`;
        let adjacentSquares = [topSquare, rightSquare, bottomSquare, leftSquare];

        let validAdjacentSquares = [];
        for (let i=0; i < adjacentSquares.length; i++) {
            let adjacentSquare = adjacentSquares[i];
            if (squareIsValid(adjacentSquare) && squareIsNotWall(adjacentSquare)) {
                validAdjacentSquares.push(adjacentSquare);
            }
        }
        return validAdjacentSquares;
    }

    return PUBLIC_METHODS;
}());