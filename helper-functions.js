function stopAllPathfindingAlgorithms() {
    DEPTH_FIRST_SEARCH.stopAlgorithm();
    BREADTH_FIRST_SEARCH.stopAlgorithm();
    A_STAR_SEARCH.stopAlgorithm();
    BI_DIRECTIONAL_SEARCH.stopAlgorithm();
    GRID.stopRenderingOptimalPath();
}

function generateRandomNumber(n1, n2) {
    return n1 + Math.floor(Math.random()*(n2 - n1+1));
}